## Ansible Galaxy

- It's a free site for finding, downloading and also sharing community sourced ansible roles.
- The ansible-galaxy command is provided as part of ansible.
- By default roles are downloaded to the path set in the config file but can be changed withe the -p option.
- Roles can have dpendencies. Those will automatically be installed.
- Don't need a profile to download. Only upload.



Link:

https://galaxy.ansible.com/



**ansible-galaxy list**

will show the installed roles.

**ansible-galaxy install geerlingguy.apache -p roles**

will install the roles and put them in the roles folder.

[ansible@elk ~]$ tree
.
├── file.retry
├── file.yml
├── line.yml
├── ~None
├── os.yml
├── ping.yml
├── roles
│   └── geerlingguy.apache
│       ├── defaults
│       │   └── main.yml
│       ├── handlers
│       │   └── main.yml
│       ├── LICENSE
│       ├── meta
│       │   └── main.yml
│       ├── README.md
│       ├── tasks
│       │   ├── configure-Debian.yml
│       │   ├── configure-RedHat.yml
│       │   ├── configure-Solaris.yml
│       │   ├── configure-Suse.yml
│       │   ├── main.yml
│       │   ├── setup-Debian.yml
│       │   ├── setup-RedHat.yml
│       │   ├── setup-Solaris.yml
│       │   └── setup-Suse.yml
│       ├── templates
│       │   └── vhosts.conf.j2
│       ├── tests
│       │   ├── README.md
│       │   └── test.yml
│       └── vars
│           ├── AmazonLinux.yml
│           ├── apache-22.yml
│           ├── apache-24.yml
│           ├── Debian.yml
│           ├── RedHat.yml
│           ├── Solaris.yml
│           └── Suse.yml
├── test.sh
└── time.yml







