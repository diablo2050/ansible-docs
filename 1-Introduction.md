## Introduction

Ansible is a client less application that uses SSH to communicate to other servers and uses the YAML syntax.

Ansible uses python and we might need to install some packages on the target machines to be able to use ansible. 

The default configuration file is **/etc/ansible/ansible.cfg** and we can override the configurations in the file or we can specify another file by:

- export ANSIBLE_CONFIG=/path/to/new/config/file
- run the ansible command.



YAML overview:

YAML stands for : Yaml Ain't Markup Language

- YAML format key=value
- Ansible uses 'spaces' not 'tabs'

```yaml
--- # shows the beginning of a configuration file.
lists or external content must be declared as such:
vars_files:
- conf/file1.yml
- conf/file2.yml
```

YAML file example: 

```yaml
---
- hosts: webservers
  vars:
    http_port: 80
    max_clients: 200
  remote_user: root
  tasks:
  - name: ensure apache is at the latest version
    yum:
      name: httpd
      state: latest
  - name: write the apache config file
    template:
      src: /srv/httpd.j2
      dest: /etc/httpd.conf
    notify:
    - restart apache
  - name: ensure apache is running (and enable it at boot)
    service:
      name: httpd
      state: started
      enabled: yes
  handlers:
    - name: restart apache
      service:
        name: httpd
        state: restarted
```

