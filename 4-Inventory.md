## Inventory

Ansible uses by default the /etc/ansible/hosts file for a server list.

Types of Inventories:

1- Static:

Are updated or changed by the user.

Example of static inventory file:

[targets]

localhost    ansible_connection=local

host2.example    ansible_connection=ssh    ansible_user=user2

We can assign variables to groups:

[atlanta]

host1

host2

[altlanta:vars]

ntp_server=ntp.atlanta.example.com

proxy=proxy.atlanta.example.com

Variables can be put in a separate file.

2- Dynamic:

Can be pulled from cloud providers or CMDB providers.