## Templates

- Templates allow you to create a file with variables inside that get replaced.
- Can use any of the Ansible variables.
- There is a template module.
- Templates are processed by the jinja2 templating engine.

Example:

```yaml
- template:
	src: /mytemplates/foo.j2
	dest: /etc/file.conf
	owner: user
	group: wheel
	mode: 0644
```

- Jinja template is simply a text file.
- A template contains **variables** and/or **expressions**, which get replaced with values when a template is rendered; and tags, which control the logic of the template. The template syntax is heavily inspired by Python.

Example:
template.j2

```html
<p>
Hello There <p>
ServerName = {{description}}
```

```yaml
---
- hosts: databases
  become: yes
  vars:
    description: "{{ansible_hostname}}"
  tasks:
  - name: write the index file.
    template: src=template.j2 dest=/var/www/html/index.html
    notify:
    - restart httpd
  - name: ensure apache is running
    service: name=httpd state=running
  handlers:
    - name: restart httpd
      service: name=httpd state=restarted
```

The above example will copy over the file the template file and put the value of the variable "description" which came from "ansible_hostname" and then will restart httpd.























