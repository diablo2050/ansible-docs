## Ad-hoc

Its something we use for a quick task.

Example:

**ansible  all -m ping**

all: to indicate all the groups and hosts in the inventory file.

-m: use module

ping: module name

**ansible web:databses -m shell -a"df -h"**

web:databses   : use the two groups

-a : command to use

**ansible web  --list-hosts**

command used to list hosts in the "web" group

**ansible -i myhosts -m ping**

-i : inventory file option

myhosts: custom inventory file.

**ansible -i myhosts -m setup**

setup: module called setup used to gather lcoal_facts

**ansible -i myhosts -m setup -a 'filter=ansible_default_ipv4'**

filter: used to filter the ipv4 block in the output

 we can put the following variable in our inventory file:

web1    folder=/home/

and then we can use it like so:

ansible -i myhosts -a "ls -l {{ folder }}"

**ansible all -m copy -a "src=/home/file.txt dest=/var/file2.txt"**

in this example we used the copy module to copy a file to the remote hosts.

**ansible all -m file -a "dest=/tmp/file3.txt mode=600"**

in this example we used the file module to create a file with the needed permissions 

**ansible web-group  -m yum -a "name=elinks state=latest" -t install -results**

in this example the results will be stored in a directory called "install-results" with each file having a separate file.