## Practical Examples:

#### 1- Deploy webservers 

we make an outline first containing all the needed steps.

-Build apache web server
-Deploy webserver with ansible user
-Ansible user needs sudo
-Leave gathering of facts on (we may run the playbook against different systems)

-Info needed:
	-apache web server package
	-distribution running on
	-apache version
	-need the web server directory customized
	-what is the site name

-What needs to be done
	-install the apache web server on the remote host.
	-create web server directory
	-web server directory has proper ownership
	-configuration of apache
		-copy http configuration file
		-set up the vhost directory.
		-add any ssl keys if needed.
	-copy the site code to the new directory
	-start the apache service
	-add/enable any modules needed
		-mysql/mariadb
		-ssl
		-mod/rewrite
	-restart the apache service

-Testing
	-using lynx or telnet, test the web service is running.

​		-wait for port 80 to be listening
	-register the output for the service status as JSON on the completion of the playbook.



#### Now we will try to make a simple playbook to accomplish the above tasks.

mywebserver.yml

```yaml
--- # deploy web server/s
- hosts: web
  user: ansible
  become: yes
  gather_facts: yes
  vars:
    apache_pkg: httpd
    distribution: redhat
    apache_version: 2.4
    apache_directory: /var/www/html/
    apache_dqdn: test.orkiservers.com
  tasks:
    - name: Install the web server package
      yum: pkg=httpd state=latest
    - name: Create the web server directory
      file: path=/var/www/html state=directory mode=644
    - name: Copy files over to the remote host
      copy: src=files/httpd.conf.template dest=/etc/httpd/conf/httpd.conf owner=root group=root mode=644
    - name: Create the vhost.d direcrtory
      file: path=/etc/httpd/vhost.d state=directory mode=755
    - name: Copy and untar the source for the site
      unarchive: src=files/sitecode.tar.gz dest=/var/www/html
    - name: Copy the vhost configuration file
      copy: src=files/default.conf.template dest=/etc/httpd/vhost.d owner=root group=root mode=644
    - name: start the web server
      service: name=httpd state=started
    - name: Test the web server
    - shell: curl http://test.orkiservers.com
      register: result
    - name: Display the result
      debug: var=result
```

Now that is a quick translation into YAML of the the outline without any optimizations.

#### Optimization of the playbook

Below are some of the optimizations we can do:

 ```yaml
--- # deploy web server/s
- hosts: web
  user: ansible
  become: yes
  gather_facts: yes
  vars:
    apache_pkg: httpd
    distribution: "{{ ansible_os_family }}"
    apache_version: 2.4
    apache_dir: /var/www/html/
    apache_fqdn: test.orkiservers.com
  tasks:
    - name: Install the web server package
      yum: pkg={{ apace_pkg }} state=latest
    - name: Create the web server directory
      file: path={{ apache_dir }} state=directory mode=644
    - name: Copy files over to the remote host
      copy: src=files/httpd.conf.template dest=/etc/httpd/conf/httpd.conf owner=root group=root mode=644
    - name: Create the vhost.d direcrtory
      file: path=/etc/httpd/vhost.d state=directory mode=755
    - name: Copy and untar the source for the site
      unarchive: src=files/sitecode.tar.gz dest={{ apache_dir }}
    - name: Copy the vhost configuration file
      copy: src=files/default.conf.template dest=/etc/httpd/vhost.d owner=root group=root mode=644
      notify:
        - StartHttpd
        - TestHttpd
        - DisplayResult
  handlers:  
    - name: StartHttpd
      service: name={{ apache_pkg }} state=started
    - name: TestHttpd
    - shell: curl http://{{ apache_fqdn }}
      register: result
    - name: DisplayResult
      debug: var=result
 ```



#### Turning it into a Role

Tree view:

webserver.yml
redhat.apachewebserver/
├── files
│   ├── default.conf.template
│   ├── httpd.conf.template
│   └── sitecode.tar.gz
├── handlers
│   └── main.yml
├── tasks
│   └── main.yml
└── vars
    └── main.yml



handlers/main.yml

```yaml
- name: StartHttpd
  service: name={{ apache_pkg }} state=started
- name: TestHttpd
- shell: curl http://{{ apache_fqdn }}
  register: result
- name: DisplayResult
  debug: var=result
```

tasks/main.yml

```yaml
- name: Install the web server package
  yum: pkg={{ apace_pkg }} state=latest
- name: Create the web server directory
  file: path={{ apache_dir }} state=directory mode=644
- name: Copy files over to the remote host
  copy: src=files/httpd.conf.template dest=/etc/httpd/conf/httpd.conf owner=root group=root mode=644
- name: Create the vhost.d direcrtory
  file: path=/etc/httpd/vhost.d state=directory mode=755
- name: Copy and untar the source for the site
  unarchive: src=files/sitecode.tar.gz dest={{ apache_dir }}
- name: Copy the vhost configuration file
  copy: src=files/default.conf.template dest=/etc/httpd/vhost.d owner=root group=root mode=644
  notify:
    - StartHttpd
    - TestHttpd
    - DisplayResult
```

vars/main.yml

```yaml
apache_pkg: httpd
distribution: "{{ ansible_os_family }}"
apache_version: 2.4
apache_dir: /var/www/html/
apache_fqdn: test.orkiservers.com
```

webserver.yml

```yaml
--- # deploy web server
- hosts: web
  user: ansible
  become: yes
  pre_tasks:
    - name: when did the role start
      raw: date > /home/ansible/start_role.log
  roles:
    -  redhat.apachewebserver
  post_tasks:
    - name: when did the role finish
      raw: date > /home/ansible/end_role.log
```



#### 2- NFS Server

First we go through the outline:

-installing and configuring an NFS static content server for web use

-installation and configuration done with the ansible user

-gathering facts on

-what we need to know?
	-distribution of system
	-the NFS server and client package names
	-path to the shared file space
	-the server/group we are installing NFS on

-what we need to do / install?
	-install the NFS server/client and utilities
	-export the shared directory (/etc/exports)
	-add our LA lab network (internal)
	-configure the filesystem export for read/write on known netowkrs.
		-disable any known or unknown network or user connectivity.	
	-start the NFS service
	-cron job to backup the filesystem
	-NFS client installs? - common role
		-NFS client /etc/fstab configuration to mount the share on boot.

-test

​	-capture the NFS server service status as a JSON output and regiterthe result

#### Now we will try to make a simple playbook to accomplish the above tasks.

mynfserver.yml

```yaml
--- # NFS Server Playbook
- hosts: appserver
  user: ansible
  become: yes
  gather_facts: yes
  vars:
    distribution: RedHat
    nfsutils_pkg: nfs-utils
    nfslibs_pkg: nfs-utils-lib
    nfsserver_service: nfs-server
    nfslock_service: nfs-lock
    nfsmap_service: nfs-idmap
    rpcbind_service: rcpbind
    export_path: /var/share
  tasks:
    - name: Install the NFS server and utilities
      yum: pkg=nfs-utils state=latest
    - name: Install the NFS server libraries
      yum: pkg=nfs-utils-lib state=latest
    - name: copy the export file to remote server
      copy: src=files/exports.remplate dest=/etc/exports ownre=root group=root mode=644
    - name: start the RPC bind service
      service: name=rcpbind state=started
    - name: start the NFS server service
      service: name=nfs-server state=started
    - name: start the lock service
      service: name=nfs-lock state=started
    - name: start the NFS map service
      service: name=nfs-idmap state=started
      
    - name: Install the NFS server and utilities
      yum: pkg=nfs-utils state=latest
      delegate_to: 127.0.0.1
    - name: Install the NFS server libraries
      yum: pkg=nfs-utils-lib state=latest
      delegate_to: 127.0.0.1
    - name: start the RPC bind service
      service: name=rcpbind state=started
      delegate_to: 127.0.0.1
    - name: start the lock service
      service: name=nfs-lock state=started
      delegate_to: 127.0.0.1
    - name: start the NFS map service
      service: name=nfs-idmap state=started
      delegate_to: 127.0.0.1
    - name: create client mount dir.
      file: path=/mnt/remote state=directory mde=755
      delegate_to: 127.0.0.1
    - name: mount the filesystmet on the client from the server
      mount: name=/mnt/remote src=/var/share fstype=nfs
      register: result
      delegate_to: 127.0.0.1
    - name: show the client mount result
      debug: var=result
```





### Now the optimization

```yaml
--- # NFS SERVER SETUP EXAMPLE
- hosts: appserver
  user: ansible
  sudo: yes
  connection: ssh
  gather_facts: yes
  vars:
    distribution: RedHat
    nfsutils_pkg: nfs-utils
    nfslibs_pkg: nfs-utils-lib
    nfsserver_service: nfs-server
    nfslock_service: nfs-lock
    nfsmap_service: nfs-idmap
    rpcbind_service: rpcbind
    export_path: /var/share
    client_path: /mnt/remote
  tasks:
  - name: Install all the NFS Server Utilities, Services and Libraries
    yum: pkg={{ item }} state=latest
    with_items:
    - "{{ nfsutils_pkg }}"
    - "{{ nfslibs_pkg }}"
  - name: Copy the export file to the remote server
    copy: src=files/exports.template dest=/etc/exports owner=root group=root mode=644
    notify: 
    - StartServerServices
    - InstallClientPackages
    - CreateClientMount
    - ClientMountServerResource
    - TestServerAvailability
  handlers:
  - name: StartServerServices
    service: name={{ item }} state=restarted
    with_items:
    - "{{ rpcbind_service }}"
    - "{{ nfsserver_service }}"
    - "{{ nfslock_service }}"
    - "{{ nfsmap_service }}"
  - name: InstallClientPackages
    yum: pkg={{ item }} state=latest
    with_items:
    - "{{ nfsutils_pkg }}"
    - "{{ nfslibs_pkg }}"
    delegate_to: 127.0.0.1
    notify: StartClientServices
  - name: StartClientServices
    service: name={{ item }} state=restarted
    with_items:
    - "{{ rpcbind_service }}"
    - "{{ nfslock_service }}"
    - "{{ nfsmap_service }}"
    delegate_to: 127.0.0.1
  - name: CreateClientMount
    file: path={{ client_path }} state=directory mode=755
    delegate_to: 127.0.0.1
  - name: ClientMountServerResource
    shell: mount -t nfs tcox5.mylabserver.com:{{ export_path }} {{ client_path }}
    register: result
    delegate_to: 127.0.0.1
  - name: TestServerAvailability
    debug: var=result
```



#### Now we can break the above playbook into roles

mkdir nfs.server

files/exports.template

```shell
	/var/share tcox3.mylabserver.com(rw,sync,no_root_squash,no_all_squash)
```

files/httpd.conf.template

```
#
# This is the main Apache HTTP server configuration file.  It contains the
# configuration directives that give the server its instructions.
# See <URL:http://httpd.apache.org/docs/2.4/> for detailed information.
# In particular, see 
# <URL:http://httpd.apache.org/docs/2.4/mod/directives.html>
# for a discussion of each configuration directive.
#
# Do NOT simply read the instructions in here without understanding
# what they do.  They're here only as hints or reminders.  If you are unsure
# consult the online docs. You have been warned.  
#
# Configuration and logfile names: If the filenames you specify for many
# of the server's control files begin with "/" (or "drive:/" for Win32), the
# server will use that explicit path.  If the filenames do *not* begin
# with "/", the value of ServerRoot is prepended -- so 'log/access_log'
# with ServerRoot set to '/www' will be interpreted by the
# server as '/www/log/access_log', where as '/log/access_log' will be
# interpreted as '/log/access_log'.

#
# ServerRoot: The top of the directory tree under which the server's
# configuration, error, and log files are kept.
#
# Do not add a slash at the end of the directory path.  If you point
# ServerRoot at a non-local disk, be sure to specify a local disk on the
# Mutex directive, if file-based mutexes are used.  If you wish to share the
# same ServerRoot for multiple httpd daemons, you will need to change at
# least PidFile.
#
ServerRoot "/etc/httpd"

#
# Listen: Allows you to bind Apache to specific IP addresses and/or
# ports, instead of the default. See also the <VirtualHost>
# directive.
#
# Change this to Listen on specific IP addresses as shown below to 
# prevent Apache from glomming onto all bound IP addresses.
#
#Listen 12.34.56.78:80
Listen 80

#
# Dynamic Shared Object (DSO) Support
#
# To be able to use the functionality of a module which was built as a DSO you
# have to place corresponding `LoadModule' lines at this location so the
# directives contained in it are actually available _before_ they are used.
# Statically compiled modules (those listed by `httpd -l') do not need
# to be loaded here.
#
# Example:
# LoadModule foo_module modules/mod_foo.so
#
Include conf.modules.d/*.conf

#
# If you wish httpd to run as a different user or group, you must run
# httpd as root initially and it will switch.  
#
# User/Group: The name (or #number) of the user/group to run httpd as.
# It is usually good practice to create a dedicated user and group for
# running httpd, as with most system services.
#
User apache
Group apache

# 'Main' server configuration
#
# The directives in this section set up the values used by the 'main'
# server, which responds to any requests that aren't handled by a
# <VirtualHost> definition.  These values also provide defaults for
# any <VirtualHost> containers you may define later in the file.
#
# All of these directives may appear inside <VirtualHost> containers,
# in which case these default settings will be overridden for the
# virtual host being defined.
#

#
# ServerAdmin: Your address, where problems with the server should be
# e-mailed.  This address appears on some server-generated pages, such
# as error documents.  e.g. admin@your-domain.com
#
ServerAdmin root@localhost

#
# ServerName gives the name and port that the server uses to identify itself.
# This can often be determined automatically, but we recommend you specify
# it explicitly to prevent problems during startup.
#
# If your host doesn't have a registered DNS name, enter its IP address here.
#
#ServerName www.example.com:80

#
# Deny access to the entirety of your server's filesystem. You must
# explicitly permit access to web content directories in other 
# <Directory> blocks below.
#
<Directory />
    AllowOverride none
    Require all denied
</Directory>

#
# Note that from this point forward you must specifically allow
# particular features to be enabled - so if something's not working as
# you might expect, make sure that you have specifically enabled it
# below.
#

#
# DocumentRoot: The directory out of which you will serve your
# documents. By default, all requests are taken from this directory, but
# symbolic links and aliases may be used to point to other locations.
#
DocumentRoot "/var/www/html"

#
# Relax access to content within /var/www.
#
<Directory "/var/www">
    AllowOverride None
    # Allow open access:
    Require all granted
</Directory>

# Further relax access to the default document root:
<Directory "/var/www/html">
    #
    # Possible values for the Options directive are "None", "All",
    # or any combination of:
    #   Indexes Includes FollowSymLinks SymLinksifOwnerMatch ExecCGI MultiViews
    #
    # Note that "MultiViews" must be named *explicitly* --- "Options All"
    # doesn't give it to you.
    #
    # The Options directive is both complicated and important.  Please see
    # http://httpd.apache.org/docs/2.4/mod/core.html#options
    # for more information.
    #
    Options Indexes FollowSymLinks

    #
    # AllowOverride controls what directives may be placed in .htaccess files.
    # It can be "All", "None", or any combination of the keywords:
    #   Options FileInfo AuthConfig Limit
    #
    AllowOverride None

    #
    # Controls who can get stuff from this server.
    #
    Require all granted
</Directory>

#
# DirectoryIndex: sets the file that Apache will serve if a directory
# is requested.
#
<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

#
# The following lines prevent .htaccess and .htpasswd files from being 
# viewed by Web clients. 
#
<Files ".ht*">
    Require all denied
</Files>

#
# ErrorLog: The location of the error log file.
# If you do not specify an ErrorLog directive within a <VirtualHost>
# container, error messages relating to that virtual host will be
# logged here.  If you *do* define an error logfile for a <VirtualHost>
# container, that host's errors will be logged there and not here.
#
ErrorLog "logs/error_log"

#
# LogLevel: Control the number of messages logged to the error_log.
# Possible values include: debug, info, notice, warn, error, crit,
# alert, emerg.
#
LogLevel warn

<IfModule log_config_module>
    #
    # The following directives define some format nicknames for use with
    # a CustomLog directive (see below).
    #
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      # You need to enable mod_logio.c to use %I and %O
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>

    #
    # The location and format of the access logfile (Common Logfile Format).
    # If you do not define any access logfiles within a <VirtualHost>
    # container, they will be logged here.  Contrariwise, if you *do*
    # define per-<VirtualHost> access logfiles, transactions will be
    # logged therein and *not* in this file.
    #
    #CustomLog "logs/access_log" common

    #
    # If you prefer a logfile with access, agent, and referer information
    # (Combined Logfile Format) you can use the following directive.
    #
    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>
    #
    # Redirect: Allows you to tell clients about documents that used to 
    # exist in your server's namespace, but do not anymore. The client 
    # will make a new request for the document at its new location.
    # Example:
    # Redirect permanent /foo http://www.example.com/bar

    #
    # Alias: Maps web paths into filesystem paths and is used to
    # access content that does not live under the DocumentRoot.
    # Example:
    # Alias /webpath /full/filesystem/path
    #
    # If you include a trailing / on /webpath then the server will
    # require it to be present in the URL.  You will also likely
    # need to provide a <Directory> section to allow access to
    # the filesystem path.

    #
    # ScriptAlias: This controls which directories contain server scripts. 
    # ScriptAliases are essentially the same as Aliases, except that
    # documents in the target directory are treated as applications and
    # run by the server when requested rather than as documents sent to the
    # client.  The same rules about trailing "/" apply to ScriptAlias
    # directives as to Alias.
    #
    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

#
# "/var/www/cgi-bin" should be changed to whatever your ScriptAliased
# CGI directory exists, if you have that configured.
#
<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    #
    # TypesConfig points to the file containing the list of mappings from
    # filename extension to MIME-type.
    #
    TypesConfig /etc/mime.types

    #
    # AddType allows you to add to or override the MIME configuration
    # file specified in TypesConfig for specific file types.
    #
    #AddType application/x-gzip .tgz
    #
    # AddEncoding allows you to have certain browsers uncompress
    # information on the fly. Note: Not all browsers support this.
    #
    #AddEncoding x-compress .Z
    #AddEncoding x-gzip .gz .tgz
    #
    # If the AddEncoding directives above are commented-out, then you
    # probably should define those extensions to indicate media types:
    #
    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz

    #
    # AddHandler allows you to map certain file extensions to "handlers":
    # actions unrelated to filetype. These can be either built into the server
    # or added with the Action directive (see below)
    #
    # To use CGI scripts outside of ScriptAliased directories:
    # (You will also need to add "ExecCGI" to the "Options" directive.)
    #
    #AddHandler cgi-script .cgi

    # For type maps (negotiated resources):
    #AddHandler type-map var

    #
    # Filters allow you to process content before it is sent to the client.
    #
    # To parse .shtml files for server-side includes (SSI):
    # (You will also need to add "Includes" to the "Options" directive.)
    #
    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

#
# Specify a default charset for all content served; this enables
# interpretation of all content as UTF-8 by default.  To use the 
# default browser choice (ISO-8859-1), or to allow the META tags
# in HTML content to override this choice, comment out this
# directive:
#
AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    #
    # The mod_mime_magic module allows the server to use various hints from the
    # contents of the file itself to determine its type.  The MIMEMagicFile
    # directive tells the module where the hint definitions are located.
    #
    MIMEMagicFile conf/magic
</IfModule>

#
# Customizable error responses come in three flavors:
# 1) plain text 2) local redirects 3) external redirects
#
# Some examples:
#ErrorDocument 500 "The server made a boo boo."
#ErrorDocument 404 /missing.html
#ErrorDocument 404 "/cgi-bin/missing_handler.pl"
#ErrorDocument 402 http://www.example.com/subscription_info.html
#

#
# EnableMMAP and EnableSendfile: On systems that support it, 
# memory-mapping or the sendfile syscall may be used to deliver
# files.  This usually improves server performance, but must
# be turned off when serving from networked-mounted 
# filesystems or if support for these functions is otherwise
# broken on your system.
# Defaults if commented: EnableMMAP On, EnableSendfile Off
#
#EnableMMAP off
EnableSendfile on

# Supplemental configuration
#
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```

and we have files/sitecode.tar.gz which contains the site files.

handlers/main.yml

```yaml
- name: StartServerServices
  service: name={{ item }} state=restarted
  with_items:
  - "{{ rpcbind_service }}"
  - "{{ nfsserver_service }}"
  - "{{ nfslock_service }}"
  - "{{ nfsmap_service }}"
```

tasks/main.yml

```yaml
- name: Install all the NFS Server Utilities, Services and Libraries
  yum: pkg={{ item }} state=latest
  with_items:
  - "{{ nfsutils_pkg }}"
  - "{{ nfslibs_pkg }}"
- name: Copy the export file to the remote server
  copy: src=exports.template dest=/etc/exports owner=root group=root mode=644
  notify:
  - StartServerServices
```

vars/main.yml

```

distribution: RedHat
nfsutils_pkg: nfs-utils
nfslibs_pkg: nfs-utils-lib
nfsserver_service: nfs-server
nfslock_service: nfs-lock
nfsmap_service: nfs-idmap
rpcbind_service: rpcbind
export_path: /var/share
client_path: /mnt/remote
```

nfsserver.yml

```yaml
--- # Master Playbook for Role Based NFS Server Deployments
- hosts: appserver
  user: test
  sudo: yes
  connection: ssh
  pre_tasks:
  - name: When did the ROLE start
    raw: date > /home/test/startofrole.log
  roles:
  - nfs.server
  post_tasks:
  - name: When did the ROLE end
    raw: date > /home/test/endofrole.log
```

And for the nfs-client:

mkdir nfs.client

files/exports.template

```shell
/var/share tcox3.mylabserver.com(rw,sync,no_root_squash,no_all_squash)
```

files/httpd.conf.template

```
#
# This is the main Apache HTTP server configuration file.  It contains the
# configuration directives that give the server its instructions.
# See <URL:http://httpd.apache.org/docs/2.4/> for detailed information.
# In particular, see 
# <URL:http://httpd.apache.org/docs/2.4/mod/directives.html>
# for a discussion of each configuration directive.
#
# Do NOT simply read the instructions in here without understanding
# what they do.  They're here only as hints or reminders.  If you are unsure
# consult the online docs. You have been warned.  
#
# Configuration and logfile names: If the filenames you specify for many
# of the server's control files begin with "/" (or "drive:/" for Win32), the
# server will use that explicit path.  If the filenames do *not* begin
# with "/", the value of ServerRoot is prepended -- so 'log/access_log'
# with ServerRoot set to '/www' will be interpreted by the
# server as '/www/log/access_log', where as '/log/access_log' will be
# interpreted as '/log/access_log'.

#
# ServerRoot: The top of the directory tree under which the server's
# configuration, error, and log files are kept.
#
# Do not add a slash at the end of the directory path.  If you point
# ServerRoot at a non-local disk, be sure to specify a local disk on the
# Mutex directive, if file-based mutexes are used.  If you wish to share the
# same ServerRoot for multiple httpd daemons, you will need to change at
# least PidFile.
#
ServerRoot "/etc/httpd"

#
# Listen: Allows you to bind Apache to specific IP addresses and/or
# ports, instead of the default. See also the <VirtualHost>
# directive.
#
# Change this to Listen on specific IP addresses as shown below to 
# prevent Apache from glomming onto all bound IP addresses.
#
#Listen 12.34.56.78:80
Listen 80

#
# Dynamic Shared Object (DSO) Support
#
# To be able to use the functionality of a module which was built as a DSO you
# have to place corresponding `LoadModule' lines at this location so the
# directives contained in it are actually available _before_ they are used.
# Statically compiled modules (those listed by `httpd -l') do not need
# to be loaded here.
#
# Example:
# LoadModule foo_module modules/mod_foo.so
#
Include conf.modules.d/*.conf

#
# If you wish httpd to run as a different user or group, you must run
# httpd as root initially and it will switch.  
#
# User/Group: The name (or #number) of the user/group to run httpd as.
# It is usually good practice to create a dedicated user and group for
# running httpd, as with most system services.
#
User apache
Group apache

# 'Main' server configuration
#
# The directives in this section set up the values used by the 'main'
# server, which responds to any requests that aren't handled by a
# <VirtualHost> definition.  These values also provide defaults for
# any <VirtualHost> containers you may define later in the file.
#
# All of these directives may appear inside <VirtualHost> containers,
# in which case these default settings will be overridden for the
# virtual host being defined.
#

#
# ServerAdmin: Your address, where problems with the server should be
# e-mailed.  This address appears on some server-generated pages, such
# as error documents.  e.g. admin@your-domain.com
#
ServerAdmin root@localhost

#
# ServerName gives the name and port that the server uses to identify itself.
# This can often be determined automatically, but we recommend you specify
# it explicitly to prevent problems during startup.
#
# If your host doesn't have a registered DNS name, enter its IP address here.
#
#ServerName www.example.com:80

#
# Deny access to the entirety of your server's filesystem. You must
# explicitly permit access to web content directories in other 
# <Directory> blocks below.
#
<Directory />
    AllowOverride none
    Require all denied
</Directory>

#
# Note that from this point forward you must specifically allow
# particular features to be enabled - so if something's not working as
# you might expect, make sure that you have specifically enabled it
# below.
#

#
# DocumentRoot: The directory out of which you will serve your
# documents. By default, all requests are taken from this directory, but
# symbolic links and aliases may be used to point to other locations.
#
DocumentRoot "/var/www/html"

#
# Relax access to content within /var/www.
#
<Directory "/var/www">
    AllowOverride None
    # Allow open access:
    Require all granted
</Directory>

# Further relax access to the default document root:
<Directory "/var/www/html">
    #
    # Possible values for the Options directive are "None", "All",
    # or any combination of:
    #   Indexes Includes FollowSymLinks SymLinksifOwnerMatch ExecCGI MultiViews
    #
    # Note that "MultiViews" must be named *explicitly* --- "Options All"
    # doesn't give it to you.
    #
    # The Options directive is both complicated and important.  Please see
    # http://httpd.apache.org/docs/2.4/mod/core.html#options
    # for more information.
    #
    Options Indexes FollowSymLinks

    #
    # AllowOverride controls what directives may be placed in .htaccess files.
    # It can be "All", "None", or any combination of the keywords:
    #   Options FileInfo AuthConfig Limit
    #
    AllowOverride None

    #
    # Controls who can get stuff from this server.
    #
    Require all granted
</Directory>

#
# DirectoryIndex: sets the file that Apache will serve if a directory
# is requested.
#
<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

#
# The following lines prevent .htaccess and .htpasswd files from being 
# viewed by Web clients. 
#
<Files ".ht*">
    Require all denied
</Files>

#
# ErrorLog: The location of the error log file.
# If you do not specify an ErrorLog directive within a <VirtualHost>
# container, error messages relating to that virtual host will be
# logged here.  If you *do* define an error logfile for a <VirtualHost>
# container, that host's errors will be logged there and not here.
#
ErrorLog "logs/error_log"

#
# LogLevel: Control the number of messages logged to the error_log.
# Possible values include: debug, info, notice, warn, error, crit,
# alert, emerg.
#
LogLevel warn

<IfModule log_config_module>
    #
    # The following directives define some format nicknames for use with
    # a CustomLog directive (see below).
    #
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      # You need to enable mod_logio.c to use %I and %O
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>

    #
    # The location and format of the access logfile (Common Logfile Format).
    # If you do not define any access logfiles within a <VirtualHost>
    # container, they will be logged here.  Contrariwise, if you *do*
    # define per-<VirtualHost> access logfiles, transactions will be
    # logged therein and *not* in this file.
    #
    #CustomLog "logs/access_log" common

    #
    # If you prefer a logfile with access, agent, and referer information
    # (Combined Logfile Format) you can use the following directive.
    #
    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>
    #
    # Redirect: Allows you to tell clients about documents that used to 
    # exist in your server's namespace, but do not anymore. The client 
    # will make a new request for the document at its new location.
    # Example:
    # Redirect permanent /foo http://www.example.com/bar

    #
    # Alias: Maps web paths into filesystem paths and is used to
    # access content that does not live under the DocumentRoot.
    # Example:
    # Alias /webpath /full/filesystem/path
    #
    # If you include a trailing / on /webpath then the server will
    # require it to be present in the URL.  You will also likely
    # need to provide a <Directory> section to allow access to
    # the filesystem path.

    #
    # ScriptAlias: This controls which directories contain server scripts. 
    # ScriptAliases are essentially the same as Aliases, except that
    # documents in the target directory are treated as applications and
    # run by the server when requested rather than as documents sent to the
    # client.  The same rules about trailing "/" apply to ScriptAlias
    # directives as to Alias.
    #
    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

#
# "/var/www/cgi-bin" should be changed to whatever your ScriptAliased
# CGI directory exists, if you have that configured.
#
<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    #
    # TypesConfig points to the file containing the list of mappings from
    # filename extension to MIME-type.
    #
    TypesConfig /etc/mime.types

    #
    # AddType allows you to add to or override the MIME configuration
    # file specified in TypesConfig for specific file types.
    #
    #AddType application/x-gzip .tgz
    #
    # AddEncoding allows you to have certain browsers uncompress
    # information on the fly. Note: Not all browsers support this.
    #
    #AddEncoding x-compress .Z
    #AddEncoding x-gzip .gz .tgz
    #
    # If the AddEncoding directives above are commented-out, then you
    # probably should define those extensions to indicate media types:
    #
    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz

    #
    # AddHandler allows you to map certain file extensions to "handlers":
    # actions unrelated to filetype. These can be either built into the server
    # or added with the Action directive (see below)
    #
    # To use CGI scripts outside of ScriptAliased directories:
    # (You will also need to add "ExecCGI" to the "Options" directive.)
    #
    #AddHandler cgi-script .cgi

    # For type maps (negotiated resources):
    #AddHandler type-map var

    #
    # Filters allow you to process content before it is sent to the client.
    #
    # To parse .shtml files for server-side includes (SSI):
    # (You will also need to add "Includes" to the "Options" directive.)
    #
    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

#
# Specify a default charset for all content served; this enables
# interpretation of all content as UTF-8 by default.  To use the 
# default browser choice (ISO-8859-1), or to allow the META tags
# in HTML content to override this choice, comment out this
# directive:
#
AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    #
    # The mod_mime_magic module allows the server to use various hints from the
    # contents of the file itself to determine its type.  The MIMEMagicFile
    # directive tells the module where the hint definitions are located.
    #
    MIMEMagicFile conf/magic
</IfModule>

#
# Customizable error responses come in three flavors:
# 1) plain text 2) local redirects 3) external redirects
#
# Some examples:
#ErrorDocument 500 "The server made a boo boo."
#ErrorDocument 404 /missing.html
#ErrorDocument 404 "/cgi-bin/missing_handler.pl"
#ErrorDocument 402 http://www.example.com/subscription_info.html
#

#
# EnableMMAP and EnableSendfile: On systems that support it, 
# memory-mapping or the sendfile syscall may be used to deliver
# files.  This usually improves server performance, but must
# be turned off when serving from networked-mounted 
# filesystems or if support for these functions is otherwise
# broken on your system.
# Defaults if commented: EnableMMAP On, EnableSendfile Off
#
#EnableMMAP off
EnableSendfile on

# Supplemental configuration
#
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```

and we have files/sitecode.tar.gz which contains the site files.

handlers/main.yml

```yaml

- name: InstallClientPackages
  yum: pkg={{ item }} state=latest
  with_items:
  - "{{ nfsutils_pkg }}"
  - "{{ nfslibs_pkg }}"
  notify: StartClientServices
- name: StartClientServices
  service: name={{ item }} state=restarted
  with_items:
  - "{{ rpcbind_service }}"
  - "{{ nfslock_service }}"
  - "{{ nfsmap_service }}"
- name: CreateClientMount
  file: path={{ client_path }} state=directory mode=755
- name: ClientMountServerResource
  shell: mount -t nfs tcox5.mylabserver.com:{{ export_path }} {{ client_path }}
  register: result
```

tasks/main.yml

```yaml

- name: Copy the export file to the remote server
  copy: src=exports.template dest=/etc/exports owner=root group=root mode=644
  notify:
  - InstallClientPackages
  - CreateClientMount
  - ClientMountServerResource
```

vars/main.yml

```yaml
distribution: RedHat
nfsutils_pkg: nfs-utils
nfslibs_pkg: nfs-utils-lib
nfsserver_service: nfs-server
nfslock_service: nfs-lock
nfsmap_service: nfs-idmap
rpcbind_service: rpcbind
export_path: /var/share
client_path: /mnt/remote
```

and the nfsclient.yml file

```yaml
--- # Master Playbook for Role Based NFS Client Deployments
- hosts: local
  user: test
  sudo: yes
  connection: ssh
  pre_tasks:
  - name: When did the ROLE start
    raw: date > /home/test/startofrole.log
  roles:
  - nfs.client
  post_tasks:
  - name: When did the ROLE end
    raw: date > /home/test/endofrole.log
```



#### 3- Database server

First the outline:

```
- Installing and Configuring a MariaDB Server (master)
- The installation will be done with the ansible user
- the installation needs to be done with sudo privileges
- the connection used is ssh
- gathering of facts needs to be on

- What do we need to know?
  - the package name of the DB server
  - the group/host of the master server
  - the directory for installing the db (if not default)
  - the version of the db
  - the distribution it is installed on

- What needs to be done/installed
  - install the MariaDB server and utilities
  - root password install
    - waitfor the db service to be started
      - takes place manually after the mysql-secure-installation run
  - install the mysql/mariadb configuration file (if needed)
  - copy the mysql/mariadb database backup to the home directory
  - import the database(s)
  - add a cron job for nightly backups
  
- Testing the db
  - run a MYSQL command and register the output as JSON format to determine success
```



**mydbserver.yml**

```yaml
--- # DATABASE DEPLOYMENT EXAMPLE
- hosts: appserver
  user: test
  sudo: yes
  connection: ssh
  gather_facts: yes
  vars:
    dbserver_pkg: mariadb-server
    dbcllient_pkg: mariadb
    dbserver_fqdn: tcox5.mylabserver.com
    dbinstalldir: /var/lib
    dbinstancename: MyDBTest
    dbdistribution: RedHat
    dbversion: 5.5.44
  tasks:
  - name: Install the MariaDB Server
    yum: pkg=mariadb-server state=latest
  - name: Install the MariaDB Client Utilities
    yum: pkg=mariadb state=latest
  - name: Start the DB Service
    service: name=mariadb state=started
  - pause: prompt="Please run the mysql_secure_installation binary and then press ENTER for the playbook to continue"
  - name: Restart the DB Service
    service: name=mariadb state=restarted
  - name: Copy the remote database in order to restore
    copy: src=files/mysqlbkup.sql dest=/var/lib/mysqlbkup.sql owner=root group=root mode=755
  - name: Create the remote database instance
    shell: mysqladmin -u root -ppassword123 create MyDBTest
  - name: Import the database backup to the live DB
    shell: mysql -u root -ppassword123 MyDBTest < /var/lib/mysqlbkup.sql
    register: result
  - debug: var=result
  - name: Add a backup CRON job to run every day at midnight
    cron: name="DBBackup" minute="0" hour="0" job="mysqldump -u root -ppassword123 --databases MyDBTest > dbbkup.sql"
  - name: Run a Quick SQL Command to be sure everything was created and is working
    shell: mysql -u root -ppassword123 -e 'SHOW DATABASES;'
    register: mysqlresult
  - debug: var=mysqlresult
```



Now the optimized version:

```yaml
--- # DATABASE DEPLOYMENT PLAYBOOK EXAMPLE
- hosts: appserver
  user: test
  sudo: yes
  connection: ssh
  gather_facts: yes
  vars:
    dbserver_pkg: mariadb-server
    dbclient_pkg: mariadb
    dbserver_fqdn: tcox5.mylabserver.com
    dbinstalldir: /var/lib
    dbinstancename: MyDBTest
    dbdistribution: RedHat
    dbversion: 5.5.44
  tasks:
  - name: Install the MariaDB Server
    yum: pkg={{ item }} state=latest
    with_items:
    - "{{ dbserver_pkg }}"
    - "{{ dbclient_pkg }}"
    notify: 
    - StartDBService
    - RunSecureInstallPause
    - RestartDBService
    - CopyBkupFile
    - CreateRemoteDB
    - DisplayCreationResults
    - RestoreRemoteDB
    - TestDBResults
    - ShowDBResults
  - name: Add a backup CRON job to run every day at midnight
    cron:  name="DBBackup" minute="0" hour="0" job="mysqldump -u root -ppassword123 --databases {{ dbinstancename }} > dbbkup.sql" 
  handlers:
  - name: StartDBService
    service: name={{ dbclient_pkg }} state=started
  - name: RunSecureInstallPause
    pause: prompt="Please Run mysql_secure_installation binary and press ENTER - Playbook will restart DB Service..."
  - name: RestartDBService
    service: name={{ dbclient_pkg }} state=restarted
  - name: CopyBkupFile
    copy: src=files/mysqlbkup.sql dest={{ dbinstalldir }}/mysqlbkup.sql owner=root group=root mode=755
  - name: CreateRemoteDB
    shell: mysqladmin -uroot -ppassword123 create {{ dbinstancename }}
    register: result
  - name: DisplayCreationResults
    debug: var=result
  - name: RestoreRemoteDB
    shell: mysql -u root -ppassword123 {{ dbinstancename }} < {{ dbinstalldir }}/mysqlbkup.sql
    register: result
  - name: TestDBResults
    shell: mysql -u root -ppassword123 -e 'SHOW DATABASES;'
    register: mysqlresult
  - name: ShowDBResults
    debug: var=mysqlresult
```



The Role version:

mkdir mariadb.server

files/mysqlbkup.sql

```
-- MySQL dump 10.14  Distrib 5.5.44-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: MyDBTest
-- ------------------------------------------------------
-- Server version	5.5.44-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `MyDBTest`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `MyDBTest` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `MyDBTest`;

--
-- Table structure for table `Persons`
--

DROP TABLE IF EXISTS `Persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Persons` (
  `PersonID` int(11) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Persons`
--

LOCK TABLES `Persons` WRITE;
/*!40000 ALTER TABLE `Persons` DISABLE KEYS */;
INSERT INTO `Persons` VALUES (123,'Smith','John','111 Main St','New York'),(124,'Smith','Jane','123 Some St','Dayton'),(125,'Wayne','Bruce','1 Wayne Manor Road','Gotham');
/*!40000 ALTER TABLE `Persons` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-26 18:33:26
```

handlers/main.yml

```yaml
- name: StartDBService
  service: name={{ dbclient_pkg }} state=started
- name: RunSecureInstallPause
  pause: prompt="Please Run mysql_secure_installation binary and press ENTER - Playbook will restart DB Service..."
- name: RestartDBService
  service: name={{ dbclient_pkg }} state=restarted
- name: CopyBkupFile
  copy: src=mysqlbkup.sql dest={{ dbinstalldir }}/mysqlbkup.sql owner=root group=root mode=755
- name: CreateRemoteDB
  shell: mysqladmin -uroot -ppassword123 create {{ dbinstancename }}
  register: result
- name: DisplayCreationResults
  debug: var=result
- name: RestoreRemoteDB
  shell: mysql -u root -ppassword123 {{ dbinstancename }} < {{ dbinstalldir }}/mysqlbkup.sql
  register: result
- name: TestDBResults
  shell: mysql -u root -ppassword123 -e 'SHOW DATABASES;'
  register: mysqlresult
- name: ShowDBResults
  debug: var=mysqlresult
```

tasks/main.yml

```yaml
- name: Install the MariaDB Server
  yum: pkg={{ item }} state=latest
  with_items:
  - "{{ dbserver_pkg }}"
  - "{{ dbclient_pkg }}"
  notify:
  - StartDBService
  - RunSecureInstallPause
  - RestartDBService
  - CopyBkupFile
  - CreateRemoteDB
  - DisplayCreationResults
  - RestoreRemoteDB
  - TestDBResults
  - ShowDBResults
- name: Add a backup CRON job to run every day at midnight
  cron:  name="DBBackup" minute="0" hour="0" job="mysqldump -u root -ppassword123 --databases {{ dbinstancename }} > dbbkup.sql"

```

vars/main.yml

```
dbserver_pkg: mariadb-server
dbclient_pkg: mariadb
dbserver_fqdn: tcox5.mylabserver.com
dbinstalldir: /var/lib
dbinstancename: MyDBTest
dbdistribution: RedHat
dbversion: 5.5.44
```

and for the main **dbserverrole.yml**

```yaml
--- # Master Playbook for Role Based DB Server Deployments
- hosts: appserver
  user: test
  sudo: yes
  connection: ssh
  gather_facts: yes
  pre_tasks:
  - name: When did the ROLE start
    raw: date > /home/test/startofrole.log
  roles:
  - mariadb.server
  post_tasks:
  - name: When did the ROLE end
    raw: date > /home/test/endofrole.log
```

