## Examples Misc.

1- Lookup

```yaml
--- # Lookup playbook example
- hosts: host1
  gather_facts: no
  tasks:
    - debug: msg="Lookup the superhero bruce wayne {{ lookup('csvfile', 'Bruce Wayne file=lookup.csf delimiter=, default=NOMATCH') }}"
```

The above playbook will lookup the value in the supplied file.

2- To run a command only once against a single host in a group:

```yaml
--- #Run once playbook example
- hosts: all
  gather_facts: no
  tasks:
    - name: run uptime command on all hosts
      raw: /usr/bin/uptime >> /home/ansible/uptime.log
    - name: list the /var directory and log it
      raw: ls -a /var >> /home/ansible.lit
      run_once: true
```

The list /var play will only run once because we added the run_once option.

3- Local actions:

```yaml
---
- hosts: 127.0.0.1
  connection: local
  become: yes
  tasks:
    - name: Install telnet client
      yum: pkg=telnet state=present
```

This will install the telnet package locally.

