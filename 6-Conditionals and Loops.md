## Conditionals and Loops

- The when statement.
- You don't need to use {{}} to use variables inside conditionals, as these are already implied.
- Until statement.

Example:

```yaml
--- 
- hosts: host1
   tasks:
     - shell: cat /etc/motd
        register: motd_contets
     - debug: msg=”stdout={{motd_contents}}”
     - debug: msg="MOTD is EMPTY"
       when: motd_contents.stdout == ""
```

The example uses the "when" statement to use the debug statement when the variable motd_contents.stdout is empty.

Example:

```yaml
---
- hosts: host_group1
  tasks:
    - name: if os is redhat install apache
      yum:
        name: httpd
        state: latest
      when: ansible_os_family == "RedHat"

```

The example will install httpd only when the variable ansible_os_family which came from ansible facts is equal to "RedHat"

Example:

```yaml
---
- hosts: host1
  vars:
    create_file: true
  tasks:
    - name: create file
      file:
        state: touch
        path: /home/ansible/variable_was_true
      when: not create_file
```

The example will create the file when the value of create_file is not true.

Example on until statement:

```yaml
--- # until example
- hosts: host1
  gather_facts: no
  tasks:
    - name: install apache
      yum: pkg=httpd state=latest
    - name: verify service status
      shell: systemctl status httpd
      register: result
      until: result.stdout.find("active (running)") != -1
      retries: 5
      delay: 5
    - debug: var=result
```

The above example will install httpd and store the result of systmectl status httpd in a variable called result, then it will keep checking the result until it finds the **active (running)** state.
retries:5  (will retry 5 times)
delay: 5 (wait 5 seconds between each try)

Example on Loops:

```yaml
---
- hosts: databases
  tasks:
    - name: multiple items - "{{ item }}"
      file:
        state: touch
        path: /home/ansible/{{ item }}
      with_items:
        - file1
        - file2
        - file3
```

The above example will create 3 files.