

## PlayBooks

 To use playbooks type the following:

 **ansible-playbook file_name.yml**

Example:

```yaml
--- 
- hosts: cpanel
  become: yes
  gather_facts: no
  tasks:
        - name: copy file to /tmp
          copy:
                src:  bash_scripts/time.sh
                dest: /orkitools/logs
                owner: root
                group: root
                mode: 0755
        - name: edit line
          lineinfile:
                path: /root/.bashrc
                line: '/usr/local/bin/info'
                state: absent
```



\- become: is used to initiate sudo because we are not using the root user

\- copy: is a module used to copy a file to the remote host.

\- lineinfile: is a module used to insert or delete a line in a file.

To test the playbook or perform a "dry run" with the **--check** flag.

Example on using user defined variables:

```yaml
---
- hosts: host1
  gather_facts: no
  vars:
    playbook_version: 0.1b
  vars_files:
    conf/file1.yml
    conf/file2.yml
  vars_prompt:
    - name: Web_Domain
      promt: Web_Domain
  tasks:
    - name: install apache
      action: yum name=httpd state=installed
      notify: Restart HTTPD
  handler:
    - name: Restart HTTPD
      action: service name=httpd state=restarted
      
```

The playbook uses a user defined variable "playbook_version" as well as two files containing more variables which are file1.yml & file2.yml and then the playbook will **ask** us for the value of the variable "Web Domain",we also used a notify and a handler which
after installing the apache the "handler" will do an action based on the "notify" section, the "notify" and "name" in the handler must be the **same**.



Both file1.yml and file2.yml might contain something like :

```yaml
---
message: Hello
author: Me
```





![](/home/amr/Pictures/Screenshot from 2018-06-11 12-14-46.png)



Example on using a variable:

```yaml
---
- hosts: host1
  tasks:
    - name: add hostname to config
       lineinfile: 
         dest: /tmp/hostname.conf
         regexp: ^Hostnamae=.*
         insertafter: ^# Hostname=
         line: Hostname={{ ansible_hostname }} 
```

The above example we are using the lineinfile module to search for the line starting with Hostname and inserting after it a line:

Hostname={{ ansible_hostname }}  where ansible_hostname is a variable from the gathered facts.



Example: 

```yaml
--- 
- hosts: host1
  tasks:
    - name:  print stdout
      command: echo  “Hello”
      register: hello
    - debug: msg=”stdout={{ hello.stdout }}”
    - debug: msg=”stderr={{ hello.stderr }}”
```

This example will echo “Hello” and store the result in a variable called ‘hell’ and print the stdout and stderr like so: 

ok: [diablo206011.mylabserver.com] => {

​    "msg": "”stdout=“Hello””"

}

ok: [diablo206011.mylabserver.com] => {

​    "msg": "”stderr=”"

}

Example:

```yaml
---
- hosts: host1
  tasks:
  - name: upload a file to the target if it doesnt exist
    copy:
      src: /opt/program1.sh
      dest: /home/ansible/program1.sh
      mode: 0755
  - name: if line not there then add it
    lineinfile:
      state: present
      dest: /home/ansible/program1.sh
      regexp: '^HOSTNAME='
      line: HOSTNAME={{ansible_hostname}}
  - name: run the program
    shell: /home/ansible/program1.sh
    register: program_output
  - debug: msg="stdout={{program_output}}"
```

The example will copy a file and make sure the hostname is updated and then run the program and print the output.

We can use **delegate_to** to run a particular play on a different host like so:

```yaml
---
- hosts: host1
  tasks:
    - name: run a ping command
      command: ping -c 4 server1 > /home/ansible/ping.out
      delegate_to: 127.0.0.1
```

The above example will use the localhost to ping server1. and save it on the localhost.



We can assign variables at the command line when using a playbook like so:

```yaml
--- 
- hosts: '{{ hosts }}'
  user: '{{ user }}'
  gather_facts: no
  tasks:
    - name: install a package
      yum: pkg={{ pkg }} state=latest
```

If i run the above playbook normally i will get the following error:

![](/home/amr/Pictures/Screenshot from 2018-06-20 08-54-14.png)

For the playbook to work we need to run it as shown below:
**ansible-playbook file.yml --extra-vars "hosts=all user=root pkg=telnet"**



#### Before we check Tags we have a similar approach like so:

**ansible-playbook file.yml --start-at-task='name of play'**

###  Tags

We can use tags to play certain parts of a playbook like so:

Example:

```yaml
---
- hosts: host1
  become: yes
  tasks:
    - name: install mysql server
      yum: name=mysqld-server state:latest
      register: there
      tags:
        - packages
    - name: start mysql
      service: name=mysqld state=started
      register: running
      tags:
        - startup
```

Then we run it by:

**ansible-playbook  file.yml --tags "starup"**

We can also skip tags:

**ansible-playbook  file.yml --skip-tags "packages"**

