## Error Handling

Example:

```bash
#!/bin/bash

echo "standard output here"
touch /etc/testfile
```

```yaml
---
- hosts: databases
  tasks:
    - name: copy file to remote server
      copy: src=test.sh dest=/home/ansible/test.sh mode=0755
    - name: run the script
      command: /home/ansible/test.sh
      register: comm_out
    - debug:
        msg="STDOUT - {{ comm_out.stdout }}"
    - debug:
        msg="STDERR - {{ comm_out.stderr }}"
```

If we run the above playbook we will get an error and the playbook will stop, we can ignore the error and let the playbook continue on like so:

```yaml
---
- hosts: databases
  tasks:
    - name: copy file to remote server
      copy: src=test.sh dest=/home/ansible/test.sh mode=0755
    - name: run the script
      command: /home/ansible/test.sh
      register: comm_out
      ignore_errors: yes
    - debug:
        msg="STDOUT - {{ comm_out.stdout }}"
    - debug:
        msg="STDERR - {{ comm_out.stderr }}"
```

Now when we run the above playbook we get output like so:

TASK [debug] **************************************************************************************************************************************************************
ok: [diablo206012.mylabserver.com] => {
    "msg": "STDOUT - standard output here"
}
ok: [diablo206013.mylabserver.com] => {
    "msg": "STDOUT - standard output here"
}

TASK [debug] **************************************************************************************************************************************************************
ok: [diablo206012.mylabserver.com] => {
    "msg": "STDERR - touch: cannot touch ‘/etc/testfile’: Permission denied"
}
ok: [diablo206013.mylabserver.com] => {
    "msg": "STDERR - touch: cannot touch ‘/etc/testfile’: Permission denied"
}



As we see we printed out the error message.

