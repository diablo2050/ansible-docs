## Roles

- Is a playbook that is split into multiple files.
- e.g One file for tasks, one for variables, one for handlers.
- Are a method you use to package up tasks, handlers and everything else you need into reusable components you put together and include in a playbook.
- Ansible Galaxy is a repository for roles people have created for tasks.

Example on a role:

We have a playbook with the following contents:

```yaml
---
- hosts: host1
  tasks:
  - shell: cat /etc/motd
  	register: motd_contents
  - debug: msg="stdout={{motd_contents}}"
  - debug: msg="MOTS is EMPTY"
    when: motd_contents.stdout == ""
```

to split the above playbook into a role we create two files:

tasks/main.yml

```yaml
---
- shell: cat /etc/motd
  register: motd_contents
- debug: msg="stdout={{motd_contents}}"
- debug: msg="MOTS is EMPTY"
  when: motd_contents.stdout == ""
```

playbook.yml

```yaml
---
- hosts: host1
  tasks:
    - include: tasks/main.yml
```

```shell
test2/
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml
```



The above  shows an example of the directory structure for a role.



Example based on the  directory structure:

tasks/main.yml

```yaml
- name: install apache
  yum: pkg=httpd state=latest
  notify: Restart HTTPD
```

handlers/main.yml

```yaml
- name: Restart HTTPD
  service: name=httpd state=restarted
```

vars/main.yml

```yaml
website_name: This is our website
```

webservers.yml

```yaml
--- # master playbook to install apache
- hosts: host1
  become: yes
  roles:
    - apache-webserver
```



Example:

create a new folder "customize-apache" and  "customize-apache/tasks"

 customize-apache/tasks/main.yml

```yaml
- name: crate a file if it doesnt exist
  file: state=touch path=/var/www/html.index.html
- name: if hostname not in the file then add it.
  lineinfile:
    state: present
    dest: /var/www/html/index.html
    line: Hostname={{ansible_hostname}}
```

 update_apache.yml

```yaml
--- # put the hostname in the index.html
- hosts: host1
  become: yes
  roles:
    - apache-webserver
    - customize-apache
```

The above example has two roles and a playbook which calls the roles.



## Parallelism

The default is Ansible can communicate with five servers at once.
**ansible all -m ping -f 10**

The above command used the **-f 10**  which uses ping to communicate with 10 servers at a time.

The **f** flag can also be used when calling playbooks like so:

**ansible-playbook name.yml -f 10**

To use it inside a playbook:

```yaml
---
- hosts: all
  become: yes
  serial: 3
  tasks:
    - name: install httpd
      yum: name=httpd state=latest
```

The above **serial** option is used to indicate in the playbook that we want the playbook to run on 3 servers at a time.

we can also use percentages like so:

```yaml
---
- hosts: all
  become: yes
  serial: "30%"
  tasks:
    - name: install httpd
      yum: name=httpd state=latest
```

The above serial option indicates that we want to run the playbook on 30% of the servers at a time.

Can also be done as a list of batches:

```yaml
---
- hosts: all
  become: yes
  serial:
    - 1
    - 5
    - "20%"
  tasks:
    - name: install httpd
      yum: name=httpd state=latest
```

The above playbook will run the playbook against the hosts in the following order:

1- one host.
2- five hosts.
3- 20% of the hosts.

And it will repeat the above list until all the hosts are used.