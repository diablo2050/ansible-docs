## Modules

This page will list some of the commonly used modules with.

Note: all the modules have more options than is used below.

ansible modules documentation URL:
https://docs.ansible.com/ansible/latest/modules/modules_by_category.html

#### 1- setup module

```bash
ansible web -m setup
# will collect all the ansible facts from the web group.
ansible web -m setup -a"filter=ansible_architecture"
# filtering the output like so:
server3 | SUCCESS => {
    "ansible_facts": {
        "ansible_architecture": "x86_64"
    }, 
    "changed": false
}
ansible web -m setup -a"filter=ansible_dist*"
# filtering for ansible_dist*
server3 | SUCCESS => {
    "ansible_facts": {
        "ansible_distribution": "CentOS", 
        "ansible_distribution_file_parsed": true, 
        "ansible_distribution_file_path": "/etc/redhat-release", 
        "ansible_distribution_file_variety": "RedHat", 
        "ansible_distribution_major_version": "7", 
        "ansible_distribution_release": "Core", 
        "ansible_distribution_version": "7.3.1611"
    }, 
    "changed": false
}

```

#### 2- file module

```shell
ansible web -m file -a"path=/etc/fstab"
# generates information on the file.
server3 | SUCCESS => {
    "changed": false, 
    "gid": 0, 
    "group": "root", 
    "mode": "0644", 
    "owner": "root", 
    "path": "/etc/fstab", 
    "secontext": "system_u:object_r:etc_t:s0", 
    "size": 473, 
    "state": "file", 
    "uid": 0
}

ansible server3 -m file -a"path=/tmp/etc state=directory mode=0700 owner=root"
# create a folder with 0700 mode.
server3 | SUCCESS => {
    "changed": true, 
    "gid": 0, 
    "group": "root", 
    "mode": "0700", 
    "owner": "root", 
    "path": "/tmp/etc", 
    "secontext": "unconfined_u:object_r:user_tmp_t:s0", 
    "size": 6, 
    "state": "directory", 
    "uid": 0
}

```

#### 3- copy module

```shell
ansible server3 -m copy -a "src=/root/file.txt dest=/tmp/file2.txt"
# this command will copy a file to the remote system.
```

```yaml
--- # copy module
- hosts: all
  gather_facts: no
  tasks:
    - name: copy file to source directory.
      copy: src=files/file1.txt dest=/tmp/file1.txt mode=0644 owner=root group=root backup=yes
```

backup=yes keeps a copy of the destination file if it exists.

#### 4- pause module

```yaml
--- # pause module
- hosts: server3
  gather_facts: no
  tasks:
    - name: Install Apache
      action: yum name=httpd state=installed
    - name: Pausing
      pause:
      	prompt: "make sure the installation is complete.."
    - name: verify telnet installation
      action: yum name=telnet state=present
```

The pause module will pause during the execution of the play.

#### 5- waitfor module

```yaml
--- # waitfor module
- hosts: server3
  gather_facts: no
  tasks:
    - name: install tomcat
      yum: name=tomcat state=installed
    - name: wait for port 8080
      wait_for:
        port: 8080
        state: started
    - name: verify telnet is installed
      yum: name=telnet state=installed
```

The playbook will pause because it is waiting for the port 8080 is open and when we start tomcat manually the playbook will continue.

#### 6- apt module

```yaml
--- # apt module
- hosts: debian1
  become: yes
  tasks:
    - name: install apache
      apt: name=apache2 state=present
```

basically same as the yum module.

```yaml
--- # apt module
- hosts: debian1
  become: yes
  tasks:
    - name: equal to apt-get update
      apt: update_cache=yes	
```



#### 7- service module 

preferred not to turn off gather_facts

```yaml
--- # service module
- hosts: web
  tasks:
    - name: install apache
      yum: name=httpd state=installed
    - name: start httpd
      service: name=httpd state=started
```

#### 8- command module

```yaml
--- # command module
- hosts: web
  tasks:
    - name: run script
      command: /home/ansible/test.sh
```

ansible will run the test.sh file

#### 9- cron module

```yaml
--- # cron module
- hosts: web
  tasks:
    - name: add cron job
      cron: name="list files" minute="0" hour="1" job="ls -al > /home/ansible/ls.out"
```

the cron job will look something like this:

#Ansible: Ansible local facts
0 1 * * * ls -al > /home/ansible/ls.out

#### 10- fetch module

```yaml
--- # fetch module
- hosts: web
  tasks:
    - name: copy file from remote host here.
      fetch: src=/etc/fstab dest=/tmp/fstab
```

#### 11- user module

```yaml 
--- # user module
- hosts: web
  tasks:
    - name: add a system user
      user: name=test comment="ansible test" group=wheel shell=/bin/bash
```

module will add a user with bash as the default shell.

#### 12- AT module

run at a certain time, we need to make sure the "at" package is installed first.

```yaml
--- # at module 
- hosts: all
  user: root
  gather_facts:  no
  tasks:
    - name: example of a future command
      at: command="ls -al /var/log > /tmp/at.log" count=1 units="minutes"
```

count=1 (means 1 unit of time)
units= what unit of time to use.

If we want to cancel the above command:

```yaml
--- # at module 
- hosts: all
  user: root
  gather_facts:  no
  tasks:
    - name: example of a future command
      at: command="ls -al /var/log > /tmp/at.log" state=absent
```

we can also use "unique=true" so if ran the same playbook multiple times we won't get multiple instances of the same command.

#### 13- setfact module

we can use custom facts in our playbook.

```yaml
--- # set_fact module example
- hosts: all
  gather_facts: no
  vars:
    playbooks_version: 0.1
  tasks:
    - name: local variable display
      set_fact:
        singlefact: anything
    - debug: msg={{ playbooks_version }}
    - debug: msg={{ singlefact }}
```

#### 14- stat module

```yaml
--- # stat module 
- hosts: all
  gather_facts: no
  tasks:
    - name: collect file stats
      stat: path=/etc/fstab
      register: result
    - debug: msg={{ result }}
```

#### 15- script module

used to run a script.

```yaml
---
- hosts: all
  gather_facts: no
  tasks:
    - name: run script
      script: /home/script.sh creates > /tmp/script.log
```

#### 16- shell, command, raw modules

All of them are used to execute commands on remote hosts, for differences check the ansible documentation.

#### 17-unarchive module

used to copy and decompress  a compressed file.

```yaml
---
- hosts: all
  gather_facts: no
  tasks:
    - name: copy and unarchive a file
      unarchive: src=files/test.tar.gz dest=/etc/unarchive

```

#### 18- htpasswd module

```yaml
---
- hosts: web1
  gather_facts: no
  tasks:
    - name: add a user to web site autherntiaction
      htpasswd: path=/etc/httpd/.htpasswd name=user1 password=secret owner=user1 group=user1 mode=0644
```

#### 19- geturl module

can be used to download a file.

```yaml
- hosts: web1
  gather_facts: no
  tasks:
    - name: download files.
      get_url: url=http://website.com/file.txt dest=/tmp/file.txt mode=0440
```

#### 20- group module

```yaml
--- 
- hosts: web1
  gather_facts: no
  tasks:
    - name: using the group module
      group: name=newgroup state=present gid=1050 
```

#### 21- mail module

the mail will be by the anisble user we are using.

```yaml
---
- hosts: web2
  gather_facts: yes
  tasks:
   - name: send mail to a user.
     mail:
       host='localhost'
       port=25
       to="root"
       subject="out host is ready for production"
       body='System called {{ ansible_hostname }} is ready for use'
```

the above playbook will send an email to the local user called "test"

#### 22-filesystem module

```yaml
---
- hosts: web1
  gather_facts: no
  tasks:
    - name: format the remote partition
      filesystem: fstype=ext4 dev=/dev/sdf opts="-cc" 
```

opts: is like the options used in mkfs command.

#### 23- mount module

```yaml
---
- hosts: web1
  gather_facts: no
  tasks:
    - name: mount the remote device
      mount: name=/mnt/data src=/dev/sdf fstype=ext4 opts=rw state=present
```

#### 24- acl module

```yaml
---
- hosts: web2
  gather_facts: no
  tasks:
    - name: get ACL info on the /etc/test.txt remote file.
      acl: name=/etc/test.txt entity=test etype=user permissions="rw" state=present
      register: aclinfo
    - debug: var={{ aclinfo }}
```

#### 25- git module

```yaml
---
- hosts: web1
  gather_facts: no
  tasks:
    - name: checkout the git repo on the remote server
      git: repo=ssh://test@test1/home/testrepo dest=/var/gitrepo
```

#### 26- mysql_db module

```yaml
---
- hosts:
  gather_facts: yes
  tasks:
    - name: crate a new test databse
      mysql_db: name=test_db state=present login_user=root login_password=password123
```

After we created the new database we can take a dump from it.

```yaml
yal---
- hosts:
  gather_facts: yes
  tasks:
    - name: take a dump from a database
      mysql_db: name=test_db state=dump target=/var/lib/dump_test.sql  login_user=root login_password=password123
      
```

#### 27- mysql_user module.

```yaml
---
- hosts:
  tasks:
    - name: install mysql python support library
      yum: name=MYsQL-python state=latest
    - name: crate a new test databse
      mysql_user: name=test password=secret priv=*.*:ALL state=present login_user=root login_password=password123
```



#### 28- find module

```yaml
--- # Find module
- hosts: server1
  become: yes
  gather_facts: yes
  tasks:
    - name: simple find on fstab file
      find: paths="/etc" patterns='fstab' recurse=yes size="12k" age="300 or 30m or 30d ..etc" regex=True
```



#### 29- package module

can be used to install a package on all distributions without using yum or apt.

```yaml
--- # Package module
- hosts: server1
  become: yes
  gather_facts: yes
  tasks:
    - name: install telnet on all distributions
      package: name=telnet state=latest
```



