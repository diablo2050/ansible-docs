## Core Components

Inventories:

- Static or local /etc/ansible/hosts
- Can be called from a different file using -i flag
- Can be dynamic, provided by a script
- can group hosts.

Modules:

- Vools ansible uses.
- Ansible comes with many modules.
- We can write our own modules.

Variables:

- Allows us to customize behavior for systems.

- Variable names should be letters, numbers and underscores.

- Should always start with a letter.

- Can be defined in the inventory file or in a playbook.

- Variables can be referenced using the Jinja 2 templating system.

- - Example: dest={{ remote_path }}

Ansible Facts:

- Way of getting data from systems.
- Can be used in playbooks.
- Can be disabled to speed up playbooks.

Playbooks:

- Playbooks are instructions used for execution.
- Made up of individual plays.
- Play is a task.
- Written in YAML format.

Configuration Files:

- default is /etc/ansible/ansible.cfg

- config file is read when a playbook is run.

- Can use config files other than the default. The order is as follows;

- - ANSIBLE_CONFIG(enviroment variable)
  - ansible.cfg (in the current directory)
  - .ansible.cfg (in the home directory)
  - /etc/ansible/ansible.cfg.

Templates:

- Is a definition and set of parameter for running a job.
- Are useful to execute same job many times.
- Variables can be used in templates to populate the content.

Handlers:

- task in a playbook can trigger a handler.
- Used to handle error conditions.
- Called at the end of each play.

Roles:

- Playbook is standalone file to set up servers.
- Roles can be thought of as playbooks that are split in files.
- A file for tasks, one for variables, one for handlers.
- Ansible Galaxy is a repo for roles pople have created.

Vault:

- Is a secure store.
- Allows us to keep sensitive data such as passwords and encrypted files.

 