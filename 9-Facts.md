## Facts

- Ansible facts is a way of getting data from systems.
- Can be used in playbook variables.
- Can be disabled in a playbook.( gather_facts: no)

**ansible group1 -m setup -a "filter=ansible_nodename"**

in the above example we filtered the nodename.

output:

diablo206012.mylabserver.com | SUCCESS => {
    "ansible_facts": {
        "ansible_nodename": "diablo206012.mylabserver.com"
    }, 
    "changed": false
}



To create custom facts:

1- create facts.d under /etc/ansible/ in the host.

2- the file should end with .fact

Example of a file.fact:

[general]
private_fact=bugs
other_fact=bunny

To view them we use:

 **ansible local -m setup -a "filter=ansible_local"**

Output:

localhost | SUCCESS => {
    "ansible_facts": {
        "ansible_local": {
            "file": {
                "general": {
                    "other_fact": "bunny", 
                    "private_fact": "bugs"
                }
            }
        }
    }, 
    "changed": false
}



To use it in a playbook:

```yaml
---
- hosts: local
  tasks:
    - debug: msg="{{ ansible_local.preference }}"
```

where preference is (/etc/ansible/facts.d/preference.fact) the filename



Example:
**ansible local -m setup -a "filter=ansible_date_time"**

Output:

localhost | SUCCESS => {
    "ansible_facts": {
        "ansible_date_time": {
            "date": "2018-06-13", 
            "day": "13", 
            "epoch": "1528876195", 
            "hour": "09", 
            "iso8601": "2018-06-13T07:49:55Z", 
            "iso8601_basic": "20180613T094955583731", 
            "iso8601_basic_short": "20180613T094955", 
            "iso8601_micro": "2018-06-13T07:49:55.583913Z", 
            "minute": "49", 
            "month": "06", 
            "second": "55", 
            "time": "09:49:55", 
            "tz": "EET", 
            "tz_offset": "+0200", 
            "weekday": "Wednesday", 
            "weekday_number": "3", 
            "weeknumber": "24", 
            "year": "2018"
        }
    }, 
    "changed": false
}



To access the elements in the output such as hour, minute:

```yaml
---
- hosts: localhost
  tasks:
    - debug: msg="The server Date {{ ansible_date_time.date }} Time {{ ansible_date_time.hour }} {{ ansible_date_time.minute }}"
```

Output:

ok: [localhost] => {

​    "msg": "The server Date 2018-06-13 Time 09 55"
}