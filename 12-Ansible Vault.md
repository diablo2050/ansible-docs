## Ansible Vault

- Is a secure store.
- It allows Ansible to keep sensitive data.
- Store Passwords.
- Encrypted files.
- Command line tool ansible-vault is used to edit files.
- Command line flag is used --ask-vault-pass or --vault-password-file.

Example:

```bash
mkdir testing
cd testing
mkdir roles
ansible-galaxy init roles/test.demo
cd roles/
vim test.demo/tasks/main.yml
```

test.demo/tasks/main.yml

```yaml
---
# tasks file for roles/test.demo
- copy: content="{{special_password}}" dest=/home/ansible/encrypted_output
```

Now the file will be copied and the content will be readable.

To use ansible-vault:

**ansible-vault encrypt test.demo/vars/main.yml** (to encrypt the file which has the stored password.)
Then we enter the vault password.
When we try to run the playbook:

ansible-playbook test-demo.yml 
**ERROR! Attempting to decrypt but no vault secrets found**

To run the playbook we use the following syntax:

**ansible-playbook test-demo.yml --ask-vault-pass**

Then we supply the password we entered in the previous command.



### ansible-vault options:

- ansible-vault edit filename.yml
- rekey filename.yml (to change the vault password.)
- ansible-vault view filename.yml (to only view the file)
- ansible-vault decrypt filename.yml (to decrypt it)
- To supply the vault password via file we use the following:
  - create a file containing the password.
  - we call the file like so: ansible-vault filename.yml --vault-password-file=path_to_file

